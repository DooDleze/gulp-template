$(document).ready(function(){

    //Главный слайдер
    $('.main-slider').owlCarousel({
        loop:true,
        margin:0,
        responsiveClass:true,
        items:1,
        nav:true,
        dots: false,
        navText: ['',''],
        navContainer: '.main-slider-nav'
        // responsive:{
        //     0:{
        //         items:1,
        //     },
        //     600:{
        //         items:1,
        //     },
        //     1000:{
        //         items:1,
        //     }
        // }
    });


    $('.toper').click(function(){
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        if( $('*').is('#one-page-scroll') ){
            $.fn.fullpage.moveTo(1);
        }
    });
    

    $('.toggle-button').click(function (e) {
        e.preventDefault();
        $(this).find('.hamburger').toggleClass('active');
        var target = $(this).data('toggle');
        $('#'+target).toggleClass('active');
    });

    $(document).on("click", '.main-menu.active li.parent > a', function(e) { 
        e.preventDefault();
        if($(this).hasClass('clicked')){
            location.href = $(this).attr('href');
        }else{
            $(this).addClass('clicked');
            if($(this).parent().hasClass('parent')){
                $(this).next().slideDown( "slow" );
            }
        }
        return false;
    });
    

    //Поиск
    $('.header .icon.search').click(function (e) { 
        e.preventDefault();
        $('.search-form-wrapper').addClass('active');
        $('.search-input').focus();
        $('.header').addClass('swhite');
    });

    $('.search-form-wrapper .search-close').click(function (e) { 
        e.preventDefault();
        $('.search-form-wrapper').removeClass('active');
        $('.header').removeClass('swhite');
    });
    
    $(".search-form").submit(function () {	//Обработка отправки
        $(".search-results").load("/search",$(".search-form").serialize()).show();//Загрузка результатов поиска от страницы search.html и показ контейнера
        return false;					//Дополнительная заглушка
    });

    $(".search-form input").keyup(function() {	//Живой поиск
        if(this.value.length > 2) {			//Пользователь набирает больше 2 символов в строке поиска
            $(".search-results").load("/search",$(".search-form").serialize()).show();//Загрузка результатов поиска от страницы search.html и показ контейнера
        }
        else {
            $(".search-results").hide();		//Если набрано меньше 2 символов, скрыть контейнер (CSS display:none;)
        }
    });

});

