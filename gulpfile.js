'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

var assetPath = './assets/js/minyfy/';

const paths = {
  scss: 'assets/scss*/**',
  css: 'assets/scss/styles.scss',
  js: 'js/**/*',
  html: '*.html'
};

gulp.task('serve', ['sass'], function() {
  browserSync.init({
    //proxy: "http://DOMAIN.dev", // Don't forget to change.
    server: "./",
    noOpen: false
  });
  gulp.watch(paths.scss, ['sass']);
  gulp.watch(paths.html).on("change", browserSync.reload);
  gulp.watch(paths.js).on('change', browserSync.reload);
});

gulp.task('sass', function () {
  return gulp.src(paths.css)
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sass().on('error', function (err) {
      console.log(err.toString());
      this.emit('end');
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./assets/css/'))
    .pipe(browserSync.stream());
});


// Задача "assets". Запускается командой "gulp assets"
gulp.task('assets', function() {

  // gulp.src(
  //   './node_modules/bootstrap/dist/**/*' // берем любые файлы в папке и ее подпапках
  // )
  // .pipe(gulp.dest(assetPath+'bootstrap')); // результат пишем по указанному адресу

  gulp.src(
    './node_modules/owl.carousel/dist/**/*'
  )
  .pipe(gulp.dest(assetPath+'owl.carousel'));


  // для ассетов без картинок
  var jsFiles = [
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
  ];

  var cssFiles = [
    './node_modules/bootstrap/dist/css/bootstrap.min.css',
  ];

  gulp.src(jsFiles)
    .pipe(concat('assets.js'))
    .pipe(gulp.dest(assetPath))
    .pipe(rename('assets.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(assetPath));

  gulp.src(cssFiles)
    .pipe(concat('assets.css'))
    .pipe(gulp.dest(assetPath));
  return true;

});

gulp.task('default', ['serve']);
